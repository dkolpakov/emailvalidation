<?php

/*
Создайте абстрактный класс EmailInformer с абстрактными методами setMail и send.

Создайте два дочерних класс GoogleInformer и YandexInformer.

Они должны отличаться тем, что метод setMail у первого принимает почту с gmail.ru, а второй yandex.ru.

Если передана неверная почта, то они сообщают об ошибке.

Метод send должен выводить на экран сообщение: "Отправлено на почту [тип(google или yandex)][почта(переданная в setMail)]".

Реализуйте объекты GoogleInformer и YandexInformer и проверьте, что они работают верно.
*/

abstract class EmailInformer {

    abstract public function setMail(string $email);

    abstract public function send();

    public function validation(string $email, string $domain): void
    {
        if ($domain == 'gmail') (bool) preg_match('/.*@gmail\.ru$/', $email);
        elseif ($domain == 'yandex') (bool) preg_match('/.*@yandex\.ru$/', $email);
    }
}

class GoogleInformer extends EmailInformer
{
    private $email;

    public function setMail(string $email): void
    {
        if ($this->validation($email, 'gmail')) {
            echo $email.' - У вас отличный email!'.'<br>';
            $this->email = $email;
        } else {
            echo $email.' - не корректный email'.'<br>';     
        }
    }

    public function send()
    {
        if ($this->email) {
            echo "Отправлено на почту yandex: ".$this->email;
        } else {
            echo 'Ваш email не корректный, отпавка не возможна';
        }
    }
}

class YandexInformer extends EmailInformer
{
    private $email;

    public function setMail(string $email): void
    {
        if ($this->validation($email, 'yandex')) {
            echo $email.' - У вас отличный email!'.'<br>';
            $this->email = $email;
        } else {
            echo $email.' - не корректный email'.'<br>';     
        }
    }

    public function send()
    {
        if ($this->email) {
            echo "Отправлено на почту yandex: ".$this->email;
        } else {
            echo 'Ваш email не корректный, отпавка не возможна';
        }
    }
}

$gmail = new GoogleInformer();
$ymail = new YandexInformer();

$gmail->setMail('mail@gmail.ru');
$gmail->send();

echo '<hr>';

$ymail->setMail('dima@yandex.ru1');
$ymail->send();